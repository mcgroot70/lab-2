/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author jgroot
 *
 */
public class Celsius extends Temperature
{
public Celsius(float t)
{
super(t);
}
public String toString()
{
// TODO: Complete this method
return this.getValue()+ " C";
}
@Override
public Temperature toCelsius() {
	return this;
}
@Override
public Temperature toFahrenheit() {
	Temperature convert = new Fahrenheit((this.getValue())*((float) 9/5) + 32);
	return convert;
}
}
