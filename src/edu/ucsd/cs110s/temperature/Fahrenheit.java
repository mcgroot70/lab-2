/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author jgroot
 *
 */
public class Fahrenheit extends Temperature
{
public Fahrenheit(float t)
{
super(t);
}
public String toString()
{
// TODO: Complete this method
return this.getValue() + " F";
}
@Override
public Temperature toCelsius() {
	Temperature converted = new Celsius((this.getValue()-32)*((float)5/9));
	return converted;
}
@Override
public Temperature toFahrenheit() {
	return this;
}
}
